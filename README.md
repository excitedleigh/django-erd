# Django ERD

Generates Entity-Relationship diagrams from your Django apps, in the form of
Graphviz `.dot` files.

Unlike most other ERD scripts, this doesn't need to be added your
`INSTALLED_APPS`.

## Installation

(Proper releases on PyPI will be coming soon!)

```shell
pip install django-erd
```

## Usage

```shell
export DJANGO_SETTINGS_MODULE=<dotted.path.to.settings>
django-erd [--app-name <app>] | dot -Tpng -o <output-file.png>
```
