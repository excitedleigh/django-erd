from setuptools import setup

setup(
    name='django-erd',
    description='Generates an Entity-Relationship diagram from a Django project',
    author='Adam Brenecki',
    author_email='adam@brenecki.id.au',
    license='MIT',
    setup_requires=["setuptools_scm>=1.11.1"],
    use_scm_version=True,
    py_modules=['django_erd'],
    include_package_data=True,
    install_requires=[
        'begins',
    ],
    entry_points={
        'console_scripts': ['django-erd=django_erd:main'],
    }
)
